<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CompanyController extends Controller
{
    //
    public function index(){
        $data = [];
        return view('companies.index', $data);
    }
    public function add(){
        $data = [];
        return view('companies.add', $data);
    }
}
