<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\CompanyController;
Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::get('/companies/list', [CompanyController::class, 'index'])->middleware(['auth'])->name('companies.list');
Route::get('/companies/add', [CompanyController::class, 'add'])->middleware(['auth'])->name('companies.add');

require __DIR__.'/auth.php';
