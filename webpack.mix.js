const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js').postCss('resources/css/app.css', 'public/css', [
    require('postcss-import'),
    require('tailwindcss'),
    require('autoprefixer'),
]);


mix.js('resources/js/custom.js', 'public/js')
    .postCss('node_modules/datatables.net-dt/css/jquery.dataTables.css','public/css/dataTables')
    .sass('resources/scss/main.scss','public/css')
    .styles([
        'public/css/app.css',
        'public/css/dataTables/jquery.dataTables.css',
        'public/css/main.css'
    ], 'public/css/all.css')
    .scripts([
       'public/js/app.js',
       'public/js/custom.js'
    ],'public/js/all.js');
