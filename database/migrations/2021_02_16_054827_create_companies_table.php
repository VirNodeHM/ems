<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('company_name',191);
            $table->string('company_mail',191)->nullable();
            $table->string('company_address',191)->nullable();
            $table->string('company_city',191)->nullable();
            $table->string('company_state',191)->nullable();
            $table->string('company_zip',191)->nullable();
            $table->string('company_phone',191)->nullable();
            $table->string('company_fax',191)->nullable();
            $table->string('company_representative')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->date('creation_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
